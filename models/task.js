'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Task extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Task.belongsTo(models.Teacher)
      Task.belongsTo(models.Parent)
    }
  };

  Task.init({
    TeacherId: DataTypes.INTEGER,
    ParentId: DataTypes.INTEGER,
    mata_pelajaran: DataTypes.STRING,
    hari: DataTypes.STRING,
    waktu: DataTypes.STRING,
    report_link: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Task',
  });
  return Task;
};