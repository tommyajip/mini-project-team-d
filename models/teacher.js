'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  class Teacher extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Teacher.hasMany(models.Order)
      Teacher.hasMany(models.Rate)
      Teacher.hasMany(models.Task)
    }

    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    static register = ({ nama, email, password, no_telfon, pendidikan, tingkat, mata_pelajaran, biaya, provinsi, kabupaten, kecamatan, kelurahan, alamat}) => {
      const encryptedPassword = this.#encrypt(password);
      const t_role = "teacher";

      return this.create({ nama, email, password : encryptedPassword, no_telfon, pendidikan, tingkat, mata_pelajaran, role : t_role, biaya, provinsi, kabupaten, kecamatan, kelurahan, alamat})
    }

    /* Method .compareSync digunakan untuk mencocokkan plaintext dengan hash. */
    checkPassword = password => bcrypt.compareSync(password, this.password)
    
    static authenticate = async ({ email, password })=>{
      try{
        const user = await this.findOne({ where: { email } })
        if (!user) return Promise.reject("Teacher's account not found !!")
        
        const isPasswordValid = user.checkPassword(password)
        if (!isPasswordValid) return Promise.reject("Wrong password")
        
        return Promise.resolve(user)
      }catch(err){
        return Promise.reject(err)
      }
    }

  };
  Teacher.init({
    nama: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    avatar: DataTypes.STRING,
    no_telfon: DataTypes.STRING,
    pendidikan: DataTypes.STRING,
    tingkat: DataTypes.STRING,
    mata_pelajaran: DataTypes.STRING,
    role: DataTypes.STRING,
    metodologi: DataTypes.STRING,
    latar_belakang: DataTypes.STRING,
    biaya: DataTypes.INTEGER,
    provinsi: DataTypes.STRING,
    kabupaten: DataTypes.STRING,
    kecamatan: DataTypes.STRING,
    kelurahan: DataTypes.STRING,
    alamat: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Teacher',
  });
  return Teacher;
};