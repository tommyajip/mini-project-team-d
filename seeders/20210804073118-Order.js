'use strict';

const STATUS = {
  PENDING: "pending",
  HIRED: "hired",
  COMPLETED: "completed",
  CANCELED: "canceled"
}

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.bulkInsert('Orders', [{
      TeacherId: 1,
      ParentId: 1,
      status: STATUS.HIRED,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      TeacherId: 1,
      ParentId: 2,
      status: STATUS.HIRED,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      TeacherId: 6,
      ParentId: 1,
      status: STATUS.HIRED,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      TeacherId: 6,
      ParentId: 2,
      status: STATUS.HIRED,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      TeacherId: 8,
      ParentId: 1,
      status: STATUS.HIRED,
      createdAt: new Date(),
      updatedAt: new Date()
    }, 
    {
      TeacherId: 5,
      ParentId: 2,
      status: STATUS.HIRED,
      createdAt: new Date(),
      updatedAt: new Date()
    }, 
    {
      TeacherId: 9,
      ParentId: 1,
      status: STATUS.PENDING,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      TeacherId: 4,
      ParentId: 1,
      status: STATUS.PENDING,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});

  },

  down: async (queryInterface, Sequelize) => {

    await queryInterface.bulkDelete('Orders', null, {});

  }
};