$(':radio').change(function () {
    console.log('New star rating: ' + this.value);
});

//================================

function setuju(id) {
    $.ajax({
        url: `/parent/hire/${id}`,
        type: 'PUT',
        data: $('form').serialize(),
        success: res => {
            location.reload();
        }
    })
}

function completed(id) {
    $.ajax({
        url: `/parent/complete/${id}`,
        type: 'PUT',
        // data: $('form').serialize(),
        success: res => {
            location.reload();
            // giveRate(teacher_id)
        }
    })
}

function giveRate(id) {
    $.ajax({
        url: `/parent/rating/${id}`,
        type: 'GET',
        success: _=>{
            window.location.href = '/parent/rating/'+id;
        }
    })
}

function cancelled(id) {
    $.ajax({
        url: `/parent/cancel/${id}`,
        type: 'PUT',
        data: $('form').serialize(),
        success: res => {
            location.reload();
        }
    })
}

//===============================

$(document).ready(function(){
    $('.akhir-kerja').each((id , element) => {
        element.addEventListener('click', e => {
            let orderId = element.dataset.order;
            let teacherId = element.dataset.teacher;
            console.log(orderId+" dan "+teacherId);
            $("#OrderId").val( orderId );
            $("#teacher").val( teacherId );
            $('#staticBackdrop').modal('show');
        })
    });

    function validateComment(){
        let comValue = $('#comment').val();
        if(comValue == ''){
            $('#comcheck').html('Ulasan anda masih kosong');
            return false   
        }else{
            $('#comcheck').html('');
            return true;
        }
    }

    function validateRate(){
        var isValid = $("input[name=stars]").is(":checked");
        
        if(!isValid){
            $('#ratecheck').html('Wajib Kasih Penilaian');
            return false
        }else{
            $('#ratecheck').html('');
            return true
        }
    }

    function kirimUlasan(id){
        $.ajax({
            url: `/parent/rating`,
            type: 'POST',
            data: $('form').serialize(),
            success: res => {
                completed(id);
            }
        })
    }
    
    $('#kirim-ulasan').click( e => {
        e.preventDefault();
        let id = document.getElementById("OrderId").value;
        console.log("order id = "+id);

        let valid_rate = validateRate();
        let valid_comment = validateComment();
        if( (valid_rate == true) && (valid_comment == true) ){
            kirimUlasan(id);
        }else{
            return false;
        }
        
    });
});