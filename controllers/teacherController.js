const {
  Teacher,
  Parent,
  Order,
  Rate,
  Task
} = require("../models");
const bcrypt = require("bcrypt");
var passport = require("passport");
const sequelize = require("sequelize");
const Op = sequelize.Op;

class teacherController {
  static showTeachers() {
    Teacher.findAll({}).then((teachers) => {
      let teacherArr = [];
      for (const teacher of teachers) {
        teacherArr.push({
          nama: teacher.dataValues.nama,
          email: teacher.dataValues.email,
          password: teacher.dataValues.password,
          no_telfon: teacher.dataValues.no_telfon,
          pendidikan: teacher.dataValues.pendidikan,
          tingkat: teacher.dataValues.tingkat,
          mata_pelajaran: teacher.dataValues.mata_pelajaran,
          role: teacher.dataValues.role,
          biaya: teacher.dataValues.biaya,
          provinsi: teacher.dataValues.provinsi,
          kabupaten: teacher.dataValues.kabupaten,
          kecamatan: teacher.dataValues.kecamatan,
          kelurahan: teacher.dataValues.kelurahan,
          alamat: teacher.dataValues.alamat,
        });

      }
      return teacherArr
    });

  }

  static register(req, res, next) {
    Teacher.register(req.body).then(() => {
      res.redirect('/')
    }).catch(err => next(err))
  }

  static login(req, res) {
    passport.authenticate("teacher", {
      successRedirect: "/teacher/dashboard", // GET
      failureRedirect: "/teacher/login", // GET
      failureFlash: true
    })(req, res);
  }

  static logout(req, res) {
    req.logout();
    res.redirect('/');
  }

  static getLoginPage(req, res) {
    res.render("login_teacher");
  }

  static getDashboardPage(req, res) {
    const currentUser = req.user.nama;
    res.render("dashboard_teacher", {
      currentUser
    })
  }

  static getKegiatanPage(req, res) {
    // const currentUser = req.user.nama;
    Task.findAll({
      where: {
        TeacherId: req.user.id
      },
      include: [{
        model: Parent
      }],
      order: [
        ['id', 'DESC'],
      ]
    }).then(tasks => {
      console.log()
      res.render('./teacher/kegiatan_teacher', {
        tasks
      })
    })
  }


  static allTeacher(req, res) {
    Teacher.findAll({
      order: [
        ['id', 'ASC'],
      ],
    }).then(teachers => {
      res.json({
        'status': 200,
        'data': teachers
      })
      // res.render('user/inbox', {title, parent})
      // const currentUser = req.user.nama;
      // res.render('dashboard_parent', { currentUser, teachers })
    }).catch(err => next(err))
  }

  static allTeacherAPI(req, res) {
    const mapel = req.params.mapel;
    let queryMapel = null;
    const lokasi = req.params.lokasi;
    let queryLokasi = null;
    const queryArray = [];
    if ((mapel != undefined) && (lokasi != undefined)) {
      queryMapel = sequelize.where(
        sequelize.fn('lower', sequelize.col('mata_pelajaran')), {
          [Op.substring]: mapel.toLowerCase()
        }
      );
      queryLokasi = sequelize.where(
        sequelize.fn('lower', sequelize.col('kabupaten')), {
          [Op.substring]: lokasi.toLowerCase()
        }
      );
      queryArray.push(queryMapel);
      queryArray.push(queryLokasi);
    } else if ((mapel != undefined) && (lokasi == undefined)) {
      queryMapel = sequelize.where(
        sequelize.fn('lower', sequelize.col('mata_pelajaran')), {
          [Op.substring]: mapel.toLowerCase()
        }
      );
      queryArray.push(queryMapel);
    } else if ((mapel == undefined) && (lokasi != undefined)) {
      queryLokasi = sequelize.where(
        sequelize.fn('lower', sequelize.col('kabupaten')), {
          [Op.substring]: lokasi.toLowerCase()
        }
      );
      queryArray.push(queryLokasi);
    }

    Teacher.findAll({
      // attributes: ['id','avatar', 'nama', 'mata_pelajaran', 'biaya', 'kecamatan', 'kelurahan',],
      include: {
        model: Rate
      },
      order: [
        ['id', 'ASC'],
      ],
      where: queryArray
    }).then(teachers => {
      res.json({
        teachers
      })
    }).catch(err => {
      res.json({
        'error': err
      })
    })
  }

  static allTeacherHargaAPI(req, res) {
    const from = req.params.from;
    const to = req.params.to;
    let queryBetween = null;
    const maks = req.params.maks;
    let queryMaks = null;

    const queryArray = [];
    if ((from != undefined) && (to != undefined)) {
      queryBetween = {
        biaya: {
          [Op.between]: [from, to]
        }
      };
      queryArray.push(queryBetween);
    } else if (maks != undefined) {
      queryMaks = {
        biaya: {
          [Op.gt]: maks
        }
      };
      queryArray.push(queryMaks);
    }

    Teacher.findAll({
      // attributes: ['id','avatar', 'nama', 'mata_pelajaran', 'biaya', 'kecamatan', 'kelurahan',],
      include: {
        model: Rate
      },
      order: [
        ['id', 'ASC'],
      ],
      where: queryArray
    }).then(teachers => {
      res.json({
        teachers
      })
    }).catch(err => {
      res.json({
        'error': err
      })
    })
  }

  static detail(req, res) {
    const id = req.params.id
    Teacher.findOne({
      where: {
        id: id
      }
    }).then(teacher => {
      res.json({
        'status': 200,
        'data': teacher
      })
    }).catch(err => {
      res.json({
        'status': 400,
        'message': err
      })
    })
  }

  static allTeacherRateAPI(req, res) {
    Rate.findAll({
      attributes: ["TeacherId", "Teacher.id", [Rate.sequelize.fn('AVG', Rate.sequelize.col('rate')), 'rating']],
      include: [{
        model: Teacher,
        required: true,
        attributes: []
      }],
      group: 'TeacherId',
      order: [
        ['TeacherId', 'ASC']
      ]
    }).then(teachers => {
      res.json({
        teachers
      })
    }).catch(err => {
      res.json({
        'error': err
      })
    })
  }

  static getRatingAPI(req, res) {
    const id = req.params.id
    Rate.findAll({
      attributes: [
        [Rate.sequelize.fn('AVG', Rate.sequelize.col('rate')), 'rating']
      ],
      include: [{
        model: Teacher,
        attributes: []
      }],
      group: 'TeacherId',
      where: {
        TeacherId: id
      }
    }).then(rate => {
      if (rate == "") {
        res.json({
          rate: 'kosong'
        })
      } else {
        res.json({
          rate
        })
      }
    }).catch(err => {
      res.json({
        'error': err
      })
    })
  }

  static edit(req, res) {
    const id = req.user.id
    Teacher.findOne({
      where: {
        id: id
      }
    }).then(teacher => {
      res.render('teacher/edit_teacher', {
        teacher
      })
    }).catch(err => {
      res.json({
        'status': 400,
        'message': err
      })
    })
  }

  static update(req, res) {
    const id = req.params.id
    Teacher.update({
      nama: req.body.nama,
      email: req.body.email,
      avatar: req.body.avatar,
      no_telfon: req.body.no_telfon,
      pendidikan: req.body.pendidikan,
      tingkat: req.body.tingkat,
      mata_pelajaran: req.body.mata_pelajaran,
      metodologi: req.body.metodologi,
      latar_belakang: req.body.latar_belakang,
      biaya: req.body.biaya,
      provinsi: req.body.provinsi,
      kabupaten: req.body.kabupaten,
      kecamatan: req.body.kecamatan,
      kelurahan: req.body.kelurahan,
      alamat: req.body.alamat,
    }, {
      where: {
        id: id
      }
    }).then(teacher => {
      res.send('/teacher/dashboard')
    }).catch(err => {
      res.json({
        'status': 400,
        'message': err
      })
    })
  }

  static delete(req, res) {
    const id = req.params.id;
    Teacher.destroy({
      where: {
        id: id
      }
    }).then(() => {
      res.json({
        'status': 200,
        'message': 'success deleting'
      })
    }).catch(err => {
      res.json({
        'status': 400,
        'message': err
      })
    })
  }

  static dashboard(req, res) {
    Order.findAll({
      where: {
        TeacherId: req.user.id
      },
      include: [{
        model: Parent
      }],
      order: [
        ['id', 'DESC'],
      ]
    }).then(orders => {
      res.render('dashboard_teacher', {
        orders
      })
    }).catch(err => next(err))

  }

  static rating(req, res) {
    Rate.findAll({
      where: {
        TeacherId: req.user.id
      },
      include: [{
        model: Teacher
      }, {
        model: Parent
      }],
      order: [
        ['id', 'DESC']
      ]
    }).then(rates => {
      res.render('teacher/rating', {
        rates
      });
    }).catch(err => {
      console.log(err)
    })
  }

}

module.exports = teacherController;