// router.js
const router = require('express').Router()
// Controllers
const parent = require('../controllers/parentController')
const order = require('../controllers/orderController')
const teacher = require('../controllers/teacherController')
const middleware = require('../middlewares/middlewares')

// Register Page
router.get('/register', (req, res) => res.render('register'))
router.post('/register', parent.register)

router.get('/login', (req, res) => res.render('login_parent'))
router.post('/login', parent.login)

// daerah wajib penggunaan middlewares
router.get('/all-teacher', parent.getTeacherAll);

router.get('/logout', middleware.restrictParent, parent.logout)
router.get('/dashboard', middleware.restrictParent, parent.dashboard)
router.put('/hire/:id', middleware.restrictParent, order.setHire)
router.put('/complete/:id', middleware.restrictParent, order.setCompleted)
router.put('/cancel/:id', middleware.restrictParent, order.setCancelled)

router.get('/profil', middleware.restrictParent, parent.edit);
router.get('/all-parent', middleware.restrictParent, parent.allParent);

router.get('/eksplorasi', middleware.restrictParent, parent.getEksplorePage)
router.get("/eksplorasi/:id", middleware.restrictParent, parent.getTeacherDetail);

router.get('/api-teacher', middleware.restrictParent, teacher.allTeacherAPI);
router.get('/api-teacher-lokasi/:lokasi', middleware.restrictParent, teacher.allTeacherAPI);
router.get('/api-teacher-mapel/:mapel', middleware.restrictParent, teacher.allTeacherAPI);
router.get('/api-teacher/:mapel/:lokasi', middleware.restrictParent, teacher.allTeacherAPI);

router.get('/api-teacher-harga/', middleware.restrictParent, teacher.allTeacherHargaAPI);
router.get('/api-teacher-harga/:maks', middleware.restrictParent, teacher.allTeacherHargaAPI);
router.get('/api-teacher-harga/:from/:to', middleware.restrictParent, teacher.allTeacherHargaAPI);

router.post('/rating', middleware.restrictParent, parent.addRating);
router.get('/rating/:id', middleware.restrictParent, parent.giveRating);

router.get('/kegiatan', middleware.restrictParent, parent.getKegiatanPage)

router.get("/eksplorasi/booking/:id", middleware.restrictParent, order.createOrder);

router.get('/all-parent', middleware.restrictParent, parent.allParent);
router.get('/:id', middleware.restrictParent, parent.detail);
router.put('/:id', middleware.restrictParent, parent.update);
router.delete('/:id', middleware.restrictParent, parent.delete);



module.exports = router;