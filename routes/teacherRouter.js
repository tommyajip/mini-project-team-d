// router.js
const router = require("express").Router();

// Controllers
const teacher = require("../controllers/teacherController");
const order = require('../controllers/orderController')
const task = require('../controllers/taskController')


const middleware = require("../middlewares/middlewares");


router.get("/register", (req, res) => {
  res.render("register_teacher")
});
router.post("/register", teacher.register);

router.get("/login", teacher.getLoginPage);
router.post("/login", teacher.login);

// daerah wajib penggunaan middlewares
router.get("/dashboard", middleware.restrictTeacher, teacher.dashboard);
router.get('/profil', middleware.restrictTeacher, teacher.edit);

router.get('/all-teacher', middleware.restrictTeacher, teacher.allTeacher);
router.get('/logout', middleware.restrictTeacher, teacher.logout);

router.get('/kegiatan', middleware.restrictTeacher, teacher.getKegiatanPage);
router.get('/kegiatan/:id', middleware.restrictParent, task.delete)

router.get('/form_kegiatan', middleware.restrictTeacher, order.ParentList);
router.post('/form_kegiatan', middleware.restrictTeacher, task.add);

router.get('/form_kegiatan/:id', middleware.restrictTeacher, task.edit);
router.put('/form_kegiatan/:id', middleware.restrictTeacher, task.update);




router.get('/ulasan', middleware.restrictTeacher, teacher.rating)

router.get('/:id', middleware.restrictTeacher, teacher.detail);
router.put('/:id', middleware.restrictTeacher, teacher.update);
router.delete('/:id', middleware.restrictTeacher, teacher.delete);


module.exports = router;